import strutils, strformat
import os, osproc
import tables


type
  BuiltinFun = proc (args: seq[string]): int


proc splitLine(line: string): seq[string] = 
  # TODO: quoting and backslash escaping
  for token in line.split():
    result.add(token)


template logError(body: string): untyped = 
  ## Writes `body` to stderr with "blosh: " prefix
  stderr.writeLine("blosh: " & body)


# Built-in functions of the shell
var builtins = newTable[string, BuiltinFun]()

template cmd(names: varargs[string], body: untyped): untyped = 
  ## A simple template to simplify implementing new shell builtins
  for name in names:
    builtins[name] = proc (args: seq[string]): int = 
      var args {.inject.} = args
      body


cmd "help":
  echo "Benzands' Blosh shell"
  echo "Type program names and arguments, and hit enter"
  echo "Following functions are built-in:"
  for name in builtins.keys():
    echo name
  echo "Use the man command for information on other programs"


cmd "cd":
  ## Changes the current directory
  if args.len < 2:
    setCurrentDir(getHomeDir())
  else:
    let dir = args[1]
    try:
      setCurrentDir(absolutePath(expandTilde(dir)))
    except:
      logError(&"can't change directory to {dir}")


cmd "exit", "quit", "bye": quit()


proc launch(args: seq[string]): int = 
  ## Launches a process
  let prc = startProcess(
    args[0], "", args[1..^1], options = {poUsePath, poParentStreams}
  )
  let exitCode = waitForExit(prc)


proc execute(args: seq[string]): int = 
  ## Executes program from arguments and returns exit status
  if args.len == 0: return
  
  let builtin = builtins.getOrDefault(args[0])
  if not builtin.isNil():
    # Return exit code of the builtin command
    return builtin(args)
  
  # Return exit code of the process
  return launch(args)


proc loop = 
  ## Shell REPL
  var 
    line: string
    args: seq[string]
    status: int
  
  # While we have no errors
  while status == 0:
    # Prompt
    stdout.write(&"{getCurrentDir()} > ")
    line = stdin.readLine()
    args = splitLine(line)
    status = execute(args)

when isMainModule:
  loop()